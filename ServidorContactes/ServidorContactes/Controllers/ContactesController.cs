﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServidorContactes.Controllers
{
    public class ContactesController : ApiController
    {
        MySQLConnection conn = new MySQLConnection(@"server=localhost;userid=root;password=;database=contactes");
        // GET: api/Contactes
        public String Get()
        {
            return conn.allUsers();
        }

        // GET: api/Contactes/5
        public string Get(int id)
        {
            return conn.getUser(id.ToString());
        }
    }
}
