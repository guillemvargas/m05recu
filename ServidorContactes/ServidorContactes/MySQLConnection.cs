﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ServidorContactes
{
    public class MySQLConnection
    {
        MySqlConnection conn = null;
        MySqlCommand cmd = null;
        MySqlDataReader reader = null;

        public MySQLConnection(String cs)
        {
            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
            }
            catch (Exception e) { }
        }

        public String allUsers()
        {
            String command = "SELECT * FROM contactes;";
            StringBuilder content = new StringBuilder();

            cmd = new MySqlCommand(command, conn);
            reader = cmd.ExecuteReader();
            reader.Read();
            content.Append(reader.GetString(0));
            while (reader.Read())
            {
                content.Append(@"\n");
                content.Append(reader.GetString(0));
            }
            return content.ToString();
        }

        public List<String> getContacte(String id)
        {
            String command = "SELECT id_friend FROM friends WHERE id='" + id + "';";
            List<String> list = new List<string>();
            cmd = new MySqlCommand(command, conn);
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                list.Add(reader.GetString(0));
            }
            reader.Close();
            reader.Dispose();
            return list;
        }

        public String getUser(String id)
        {
            String command = "SELECT * FROM contactes WHERE id='" + id + "';";
            String name = null;
            cmd = new MySqlCommand(command, conn);
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                name += reader.GetString(0)+" ";
                name += reader.GetString(1)+" ";
                name += reader.GetString(2)+" ";
                name += reader.GetString(3)+" ";
                name += reader.GetString(4)+" ";
                name += reader.GetString(5)+" ";
            }
            reader.Close();
            reader.Dispose();
            return name;
        }

    }
}