﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientContactes
{
    public partial class Form1 : Form
    {
        static HttpClient client = new HttpClient();
        public Form1()
        {
            InitializeComponent();
            client.BaseAddress = new Uri("http://localhost:49935/");
            string aaa = "1 MESSI LEO 12/11/1990 0:00:00 leo@fcb.com 111111111 ";
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {

        }
        static async Task<string> GetContacte(string id)
        {
            string s = null;
            HttpResponseMessage response = client.GetAsync(string.Format("api/contactes/{0}", id)).Result;
            response.EnsureSuccessStatusCode();
            s = await response.Content.ReadAsStringAsync();
            return s;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Contacte c;
                String s = "\"";
                Task<string> contask = GetContacte(txtID.Text);
                string contacte = contask.Result;
                contacte = new string(contacte.Where(a => !s.Contains(a)).ToArray());
                string[] cSplit = contacte.Split(' ');
                c = new Contacte(cSplit[0], cSplit[1], cSplit[2], cSplit[3], cSplit[5], cSplit[6]);
                txtID.Text = c.Id;
                txtCognom.Text = c.Cognom;
                txtNom.Text = c.Nom;
                txtData.Text = c.DataNaix;
                txtEmail.Text = c.Email;
                txtTel.Text = c.Telefon;
            }
            catch (Exception ex)
            {
                MessageBox.Show("La ID no és vàlida");
            }
        }
    }
}
