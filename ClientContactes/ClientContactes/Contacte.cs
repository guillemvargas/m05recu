﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientContactes
{
    class Contacte
    {
        private string id;
        private string cognom;
        private string nom;
        private string dataNaix;
        private string email;
        private string telefon;

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Cognom
        {
            get
            {
                return cognom;
            }

            set
            {
                cognom = value;
            }
        }

        public string Nom
        {
            get
            {
                return nom;
            }

            set
            {
                nom = value;
            }
        }

        public string DataNaix
        {
            get
            {
                return dataNaix;
            }

            set
            {
                dataNaix = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string Telefon
        {
            get
            {
                return telefon;
            }

            set
            {
                telefon = value;
            }
        }

        public Contacte(string id, string cognom, string nom, string dataNaix, string email, string telefon)
        {
            this.Id = id;
            this.Cognom = cognom;
            this.Nom = nom;
            this.DataNaix = dataNaix;
            this.Email = email;
            this.Telefon = telefon;
        }
    }
}
